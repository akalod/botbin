<?php


namespace Console;


class outputCLI
{
    private static $output;

    public static function success($text)
    {
        self::$output->writeln("<fg=green>$text</>");
    }
    public static function system($text)
    {
        self::$output->writeln("<fg=white>$text</>");
    }

    public static function info($text)
    {
        self::$output->writeln("<fg=yellow>$text</>");
    }

    public static function init($output){
        self::$output = $output;
    }

    public static function error($text){
        self::$output->writeln("<fg=red;options=underscore>$text</>");
    }

    public static function buy($text){
        self::$output->writeln("<fg=#000000;bg=red;options=bold>$text</>");
    }
    public static function sell($text){
        self::$output->writeln("<fg=#000000;bg=green;options=bold>$text</>");
    }

}