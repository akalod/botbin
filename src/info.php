<?php


namespace Console;


class info
{
    public $code = '';

    public $purchasable = false;
    public $sellable = false;
    public $active_price = 0;
    public $binance;
    public $data; // mum çubukları

    private $RSI = 0;
    public $RSI_VOL = 0;
    private $T3 = 0;
    private $minProfit = 0.03;

    public function __construct($code)
    {
        $this->code = $code;
    }

    public function calc()
    {
        //mum çubuklarımız
        $t = binanceCLI::candidates($this->code, logic::$period);
        foreach ($t as $c) {
            $this->data[] = $c;
        }
        $this->RSI = $this->calcRSI();
        $this->T3 = $this->calcT3();
        // echo "RSI: " . $this->RSI."\n";
        // echo "T3 : " . $this->T3."\n";

        $this->purchasable = false;
        $this->sellable = false;

        $this->active_price = binanceCLI::getPrice($this->code);


        if ($this->T3 != 0) {
            if ($this->T3 != -1) {
                $this->sellable = true;
                if (SHOW_SIGNAL)
                    outputCLI::sell(date('Y-m-d H:i:s :') . $this->code . "\t" . $this->active_price . ' SELL RSI:' . $this->RSI_VOL);
            } else {
                $this->purchasable = true;
                if (SHOW_SIGNAL)
                    outputCLI::buy(date('Y-m-d H:i:s :') . $this->code . "\t" .$this->active_price. ' BUY RSI:' . $this->RSI_VOL);
            }
        }
    }


    public function calcT3()
    {

        $data = [];

        foreach ($this->data as $candy) {
            $data[] = $candy['close'];
        }

        $t = \trader_t3($data, 8, 0.7);

        //trend değişim hesaplaması
        // if(){
        //   $t[$i-1] < ($t[$i+1]
        // }
        // 497 + 1 bir öncek.i
        // 498 + 1
        if ($t[496] < $t[497]) {
            $first = -1;
        } else {
            $first = 1;
        }
        if ($t[497] < $t[498]) {
            $last = -1;
        } else {
            $last = 1;
        }

        // soyle tetikleyici
        if ($first < $last) {
            return logic::TYPE_BUY;
        } elseif ($first > $last) {
            return logic::TYPE_SELL;
        }

        return 0;
    }

    public function calcRSI()
    {
        $LOW_RSI = 30;
        $HIGH_RSI = 70;

        $data = [];

        foreach ($this->data as $candy) {
            $data[] = $candy['close'];
        }

        $rsi = \trader_rsi($data, logic::$period);
        $rsi = array_pop($rsi);
        $this->RSI_VOL = $rsi;

        if (DEBUG) {
            outputCLI::system("$this->code\tRSI:\t$rsi");
        }
        # RSI is above 70 and we own, Satılabilir
        if ($rsi > $HIGH_RSI) {
            return logic::TYPE_BUY;
            # RSI is below 30, alınabilir
        } elseif ($rsi < $LOW_RSI) {
            return logic::TYPE_BUY;
        } else {
            return 0;
        }
    }

}
