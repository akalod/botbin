<?php namespace Console;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

/**
 * Author: Seyhan YILDIZ <syhnyldz@gmail.com>
 */
class logic extends Command
{
    private $maxTrade = 0;
    private $codeList;
    private $tmpBalance;
    private $list;
    private $includeProfitToTrade = true;
    private $notAllow = [];


    const TYPE_BUY = 1;
    const TYPE_SELL = -1;

    const MIN_TRADE_VAL = 10;

    public static $period = 15;

    public function __construct()
    {
        parent::__construct();
    }

    public function configure()
    {
        $this->setName('start')
            ->setDescription('Run babby run...');
    }

    private function checkSell($w)
    {
        if (DEBUG) {
            outputCLI::info('Sell Check Trigger');
        }
        $sellList = $this->list->getSellList($this->notAllow);

        if (count($sellList) > 0) {
            foreach ($sellList as $b) {
                if ($w->currency == $b->code) {
                    if (SIMULATION) {

                        dbLayer::operationSell($b->code, $b->active_price);
                        outputCLI::sell($b->code . "\tPrice:" . $b->active_price);

                        if ($this->includeProfitToTrade) {
                            $this->tmpBalance += ($w->amount * $b->active_price);
                        } else {
                            $this->tmpBalance += $w->amount * $w->buy_price;
                        }

                        $index = array_search($b->code, $this->notAllow);
                        unset($this->notAllow[$index]);

                    }
                    break;
                }
            }
        }
        return false;
    }

    private function checkBuy($n)
    {
        if (DEBUG) {
            outputCLI::info("BUY Check Trigger $n");
        }
        $nominalPrice = $this->tmpBalance / $n;
        // whitelistteki coinler dönecek abi al sinyali tek tek birimlerde bakılacak
        //print_r($this->notAllow);
        $buyList = $this->list->getBuyList($this->notAllow);
        if ($nominalPrice < self::MIN_TRADE_VAL) {
            outputCLI::error('Buy operation fail : ' . $nominalPrice);
            return false;
        }
        foreach ($buyList as $b) {
            $amount = $nominalPrice / $b->active_price;
            if (SIMULATION) {
                if ($b->code && is_numeric($amount)) {
                    dbLayer::operationBuy($b->code, $b->active_price, $amount);
                    outputCLI::buy($b->code . "\tAmount:" . $amount . "\tPrice:" . $b->active_price);
                    $this->tmpBalance -= $nominalPrice;
                    $this->notAllow[] = $b->code;
                }
            }
            outputCLI::info("Operation Balance:\t$nominalPrice");
            outputCLI::info("Current Balance:\t$this->tmpBalance");
            break;
        }
    }

    public function execute(InputInterface $input, OutputInterface $output)
    {

        // binance init
        binanceCLI::init(getenv('BINANCE_KEY'), getenv('BINANCE_SECRET'));
        $key = getenv('BINANCE_KEY');
        outputCLI::init($output);
        outputCLI::info("KEY: $key");

        //   $output->writeln('SECRET:'.getenv('BINANCE_SECRET'),2);
        outputCLI::system('Trader started');

        //test için
        $this->tmpBalance = 1000;

        while (true) {


            if (!SIMULATION) {
                $balances = binanceCLI::getBalances();
                $this->tmpBalance = ($balances['USDT']['available'] - $balances['USDT']['onOrder']);
            }

            // ayarları her döngüde çek
            $maxFiatLimit = dbLayer::getSetting('max_fiat_limit');
            $this->maxTrade = dbLayer::getSetting('multi_trade_limit');
            $systemStatus = dbLayer::getSetting('trade_status');
            $pn = dbLayer::getSetting('period');

            if ($pn) {
                // self::$period = $pn;
            }
            if (DEBUG) {
                outputCLI::system("Candidates Period \t" . self::$period . ' min.');
            }

            // whitelist güncelleme
            $this->codeList = dbLayer::getWhiteList();

            // listi oluştur
              $this->list = new infoList($this->codeList);

            //sistem mod durumu kontrolü
            if ($systemStatus) {
                // açık operasyon sayısı alınacak
                $openOrders = dbLayer::getOpenOrders();
                $openOrdersCount = count($openOrders);
                $this->notAllow = [];

                // önce açık varsa satış kontrolleri yapılacak
                foreach ($openOrders as $order) {
                    // tek tek satılması mantıklı mı check et
                     $this->checkSell($order);
                    // satıldı ise yeni açılacak alma kontrolü için sayıyı azalt
                    $this->notAllow[] = $order->currency;
                }

                if (DEBUG)
                    outputCLI::system("Buy check count:\t".($this->maxTrade - $openOrdersCount));

                // alım kontrolleri yapılacak
                if ($this->maxTrade > 0) {
                    for ($x = 1; $x <= ($this->maxTrade - $openOrdersCount); $x++) {
                         $this->checkBuy($this->maxTrade - $openOrdersCount - $x +1);
                    }
                }
            } else {
                outputCLI::error('Trader status off, system halted');
            }

            // herhangi bir şey yapsada yapmasada döngü devam edecek
            sleep(60);
        }
    }
}
