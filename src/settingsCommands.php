<?php namespace Console;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

/**
 * Author: Seyhan YILDIZ <syhnyldz@gmail.com>
 */
class settingsCommands extends Command
{

    public function __construct()
    {
        parent::__construct();
    }

    public function configure()
    {
        $this->setName('set')
            ->setDescription('Ayarları değiştirmeye yarar')
            ->setHelp('set trade_status 1')
            ->addArgument('key', InputArgument::REQUIRED, 'add/remove/list.')
            ->addArgument('value', InputArgument::OPTIONAL, 'Parite kodu kesik');
    }

    public function execute(InputInterface $input, OutputInterface $output)
    {

        if ($input->getArgument('key') == 'list') {
            $r = dbLayer::getSettings();
            foreach ($r as $i) {
                $output->writeln($i->key . "\t" . $i->value);
            }
        } else {

            $r = dbLayer::setSettings($input->getArgument('key'), $input->getArgument('value'));
            if ($r) {
                $output->writeln('<fg=white>Setting changed</>');
            } else {
                $output->writeln('<fg=red>ERR: can\'t change this..</>');
            }
        }

        return -1;
    }
}
