<?php


namespace Console;


class infoList
{
    public $data = []; // info object
    public $canBuyList = [];
    public $canSellList = [];
    public $balances;
    public $output;
    public $binance;

    public function getByCode($code)
    {
        foreach ($this->data as $d) {
            if ($d->code == $code)
                return $d;
        }
        return false;
    }

    public function getBuyList($notAllow = null)
    {
        if (!$notAllow)
            return $this->canBuyList;
        $t = [];
        foreach ($this->canBuyList as $n) {
            if (!in_array($n->code, $notAllow)) {
                $t[] = $n;
            }
        }
        return $t;
    }

    public function getSellList($notAllow = null)
    {
        if (!$notAllow)
            return $this->canSellList;
        $t = [];
        foreach ($this->canSellList as $n) {
            if (!in_array($n->code, $notAllow)) {
                $t[] = $n;
            }
        }
        return $t;
    }

    public function __construct($list)
    {
        $this->data = $list;
        $this->balances = new \stdClass();

        foreach ($list as $c) {
            if (DEBUG) {
                outputCLI::system($c->token . ' fetching..');
            }
            $n = new info(str_replace('/', '', $c->token));
            $n->calc();
            if (DEBUG) {
                outputCLI::info($c->token . ' calculated');
            }

            if ($n->purchasable) {
                $this->canBuyList[] = $n;
            } else if ($n->sellable) {
                $this->canSellList[] = $n;
            }
            $this->data[] = $n;
        }
    }

}