<?php namespace Console;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

/**
 * Author: Seyhan YILDIZ <syhnyldz@gmail.com>
 */
class operationCommands extends Command
{

    public function __construct()
    {
        parent::__construct();
    }

    public function configure()
    {
        $this->setName('operation')
            ->setDescription('Operation sonuçlarını gösterir')
            ->setHelp('operation profit/list')
            ->addArgument('select', InputArgument::REQUIRED, 'profit/list.');
    }

    public function execute(InputInterface $input, OutputInterface $output)
    {

        switch ($input->getArgument('select')){
            case 'list':
                $list = dbLayer::getOpenOrders();
                $output->writeln("<fg=white>CODE\t\tMiktar\t\tBirim Fiyatı\t\tToplam Fiyat\t\tAlım</>");

                foreach ($list as $l){
                    $output->writeln("<fg=white>$l->currency\t\t$l->amount\t\t$l->buy_price\t\t\t$l->buy_total\t\t\t$l->operation_start_date</>");
                }

                break;
            case 'profit':
                $profit = dbLayer::getProfit();
                $output->writeln("<fg=green>Profit\t:\t\t$profit</>");
                break;

            default:
        }
        return -1;
    }
}
