<?php


namespace Console;


class binanceCLI
{
    public static $api;

    public static function init($key, $secret)
    {
        self::$api = new \Binance\API($key, $secret);
    }

    public static function candidates($code, $period)
    {
        $p = ($period >= 60 ? $period / 60 : $period) . ($period >= 60 ? 'h' : 'm');
        //  outputCLI::system("Period: $p");
        return self::$api->candlesticks($code, $p);
    }

    public static function getPrice($code)
    {
        return self::$api->price($code);
    }

    public static function getBalances()
    {
        $ticker = self::$api->prices();// Make sure you have an updated ticker object for this to work
        return self::$api->balances($ticker);
    }

}