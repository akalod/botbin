<?php namespace Console;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

/**
 * Author: Seyhan YILDIZ <syhnyldz@gmail.com>
 */
class whitelistCommands extends Command
{

    public function __construct()
    {
        parent::__construct();
    }

    public function configure()
    {
        $this->setName('whitelist')
            ->setDescription('Trade yapılacak pariteleri ekler / çıkartır')
            ->setHelp('whitelist add ETHUSDT')
            ->addArgument('select', InputArgument::REQUIRED, 'add/remove/list.')
            ->addArgument('code', InputArgument::OPTIONAL, 'Parite kodu kesik');
    }

    public function execute(InputInterface $input, OutputInterface $output)
    {
        if ($input->getArgument('select') !== 'list' && !$input->getArgument('code')) {
            $output->writeln("ERROR: Parite kodu eksik");
            return -1;
        }

        if ($input->getArgument('select') == 'add') {
            try {
                if (dbLayer::addWhiteList($input->getArgument('code'))) {
                    $output->write($input->getArgument('code') . ' eklendi');
                }
            } catch (\Exception $e) {
                $output->write($input->getArgument('code') . ' bulunamadı');
            }
        } else if ($input->getArgument('select') == 'list') {
            $r = dbLayer::getWhiteList();
            foreach ($r as $i) {
                $output->writeln($i->token . ' ');
            }
        } else {
            dbLayer::removeWhiteList($input->getArgument('code'));
        }
        return -1;
    }
}
