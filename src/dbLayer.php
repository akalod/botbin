<?php

namespace Console;

use Illuminate\Database\Capsule\Manager as Capsule;

class  dbLayer extends \Illuminate\Database\Capsule\Manager
{
    private static $db;

    private static function createSettings()
    {
        if (!self::$db->Schema()->hasTable('settings')) {
            self::$db->schema()->create('settings', function ($table) {
                $table->string('key')->unique();
                $table->string('value');
            });

            self::table('settings')->insert(['key' => 'multi_trade_limit', 'value' => '4']);
            self::table('settings')->insert(['key' => 'max_fiat_limit', 'value' => '100']);
            self::table('settings')->insert(['key' => 'trade_status', 'value' => '0']);
            self::table('settings')->insert(['key' => 'period', 'value' => '5']);
        }
    }

    private static function createOperations()
    {
        if (!self::$db->Schema()->hasTable('operations')) {
            self::$db->schema()->create('operations', function ($table) {
                $table->string('currency');
                $table->timestamp('operation_start_date')->default(self::RAW('CURRENT_TIMESTAMP'));
                $table->timestamp('operation_end_date')->nullable();
                $table->integer('amount');
                $table->integer('buy_price');
                $table->integer('total_price');
                $table->integer('sell_price')->nullable();
                $table->integer('profit')->nullable();
                $table->timestamp('status')->default(0);
            });
        }
    }

    private static function createWhiteList()
    {
        if (!self::$db->Schema()->hasTable('whitelist')) {
            self::$db->schema()->create('whitelist', function ($table) {
                $table->string('token')->unique();
            });
            // güvenli liste carsayılan değerler
            self::addWhiteList('SUSHI/USDT');
            self::addWhiteList('HOT/USDT');
            self::addWhiteList('ONE/USDT');
            self::addWhiteList('ZIL/USDT');
            self::addWhiteList('LINK/USDT');
            self::addWhiteList('DENT/USDT');
            self::addWhiteList('LTC/USDT');
            self::addWhiteList('FET/USDT');
            self::addWhiteList('BAT/USDT');
            self::addWhiteList('IOTX/USDT');
            self::addWhiteList('OM/USDT');
            self::addWhiteList('TRX/USDT');
            self::addWhiteList('ANKR/USDT');
            self::addWhiteList('BNB/USDT');
            self::addWhiteList('XMRUSDT');
            self::addWhiteList('CHZUSDT');
            self::addWhiteList('ATOM/USDT');
            self::addWhiteList('ALGO/USDT');
            self::addWhiteList('CAKE/USDT');
        }
    }

    public static function meta()
    {
        self::createSettings();
        self::createOperations();
        self::createWhiteList();
    }


    public static function init($dbName)
    {

        self::$db = new Capsule;
        self::$db->addConnection([
            "driver" => "sqlite",
            "database" => $dbName
        ]);
        self::$db->setAsGlobal();
        self::$db->bootEloquent();
        //chek is it first?
        self::meta();

    }

    public static function get()
    {
        return self::$db;
    }

    public static function addWhiteList($code)
    {
        return self::table('whitelist')->insertGetId(['token' => $code]);
    }

    public static function getSettings()
    {
        return self::table('settings')->get();
    }

    public static function setSettings($key, $val)
    {
        return self::table('settings')->where('key', $key)->limit(1)->update(['value' => $val]);
    }

    public static function getSetting($key)
    {
        $r = self::table('settings')->where('key', $key)->first('value');
        return $r ? $r->value : false;
    }

    public static function removeWhiteList($code)
    {
        return self::table('whitelist')->where('token', $code)->delete();
    }

    public static function getWhiteList()
    {
        return self::table('whitelist')->get();
    }

    public static function getOpenOrders()
    {
        return self::table('operations')->where('status', 0)->get();
    }

    public static function getProfit()
    {
        $r = self::table('operations')->where('status', 1)->groupBy('status')->first(self::RAW('sum(profit) as total'));
        return $r ? $r->total : 0;
    }

    public static function operationBuy($code, $price, $amount)
    {
        outputCLI::system("BUY :" . $code . "\t" . $price . "\t" . $amount);
        $data = [
            'currency' => $code,
            'amount' => $amount,
            'buy_price' => $price,
            'total_price' => $price * $amount,
            'status' => 0
        ];
        print_r($data);
        return self::table('operations')->insert($data);
    }

    public static function operationSell($code, $price)
    {
        outputCLI::system("SELL:" . $code . "\t" . $price . "\t");
        $r = self::table('operations')
            ->where('status', 0)
            ->where('currency', $code)
            ->first();
        if (!$r) {
            // operasyon kaydı bulunamadı
            return false;
        }

        $profit = $r->total_price - ($r->amount * $price);
        return self::table('operations')
            ->where('status', 0)
            ->where('currency', $code)
            ->limit(1)
            ->update([
                'status' => 1,
                'sell_price' => $price,
                'profit' => $profit,
                'operation_end_date' => self::RAW('CURRENT_TIMESTAMP')
            ]);

    }

}
