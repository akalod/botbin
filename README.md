-PHP - trader eklentisi gerkeyor PECL ile kurulum-
https://www.php.net/manual/tr/trader.installation.php

RSI ve T3 indikatörü kullanıyor (kararlar T3 te test ediliyor);


_başlatma (1dk aralıkla kontrol eder / periot düzenleniyor) sonsuza kadar çalışır_

```
./run start 
```

Trade hesap listesine ekleme (list/add/remove)

```
./run whitelist add SUSHI/USDT
```

Devre Dışı bırakma (çalışırken durdurulabilir / 1 ile de tekrar açabilirsiniz)

```
./run set trade_status 0
```